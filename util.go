package main

import (
	"fmt"
	"io"
	"time"
)

type DeadlineSetter interface {
	SetReadDeadline(t time.Time) error
	SetWriteDeadline(t time.Time) error
}

func ReadSlice(r io.ByteReader) (data []uint8, err error) {
	var n byte
	n, err = r.ReadByte()
	if err != nil {
		err = fmt.Errorf("when reading slice length: %w", err)
		return
	}
	for i := byte(0); i < n; i++ {
		var b byte
		b, err = r.ReadByte()
		if err != nil {
			err = fmt.Errorf("slice too short: %w", err)
			return
		}
		data = append(data, b)
	}
	return
}

func WriteSlice(w io.ByteWriter, s []byte) error {
	l := len(s)
	if l > 255 { return fmt.Errorf("slice too long: %v %#v", l, s)}
	
	if err := w.WriteByte(byte(l)); err != nil {
		return err
	}
	for _, b := range s {
		if err := w.WriteByte(b); err != nil {
			return err
		}
	}
	return nil
}
