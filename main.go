package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"time"
)

func main() {
	var port int
	if port_raw, ok := os.LookupEnv("PORT"); !ok {
		log.Printf("Use env PORT=1234 to specify port to listen on")
		port = 8080
	} else {
		var err error
		port, err = strconv.Atoi(port_raw)
		if err != nil {
			panic(err)
		}
	}
	host, ok := os.LookupEnv("HOST")
	if !ok {
		host = "127.0.0.8"
	}
	ip := net.ParseIP(host)
	if ip == nil {
		panic(fmt.Errorf("%v is not valid hostname", host))
	}

	ln, err := net.ListenTCP("tcp", &net.TCPAddr{IP: ip, Port: port})
	if err != nil {
		panic(err)
	}
	log.Printf("Listening on %v", ln.Addr())
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Print("Error:", err)
			continue
		}
		go func() {
			logger := log.New(log.Writer(), fmt.Sprintf("%v> ", conn.RemoteAddr()), log.Flags())
			logger.Print("Connect")
			defer func() {
				logger.Print("Disconnect")
				if err := conn.Close(); err != nil {
					logger.Printf("Error while disconnecting: %v", err)
				}
			}()

			place := Counter{Count:0}
			err := HandleConnection(&place, logger, conn, bufio.NewReader(conn), bufio.NewWriter(conn), 5*time.Second)
			if err != nil {
				logger.Printf("Error: %v", err)
			}
		}()
	}
}

type Counter struct {
	Count uint8
	*log.Logger
}

func (c *Counter) Current() Room {
	s := make([]byte, 1)
	s[0] = byte(c.Count)
	fmt.Printf("Current, before %v, ", c.Count)
	return s
}

func (c *Counter) Move(dir MoveDir) {
	fmt.Printf("Move %v, before %v, ", dir, c.Count)
	switch dir {
	case U:
		c.Count++
	case D:
		c.Count--
	}
	fmt.Printf("after %v\n", c.Count)
}

func (c *Counter) Teleport(room Room) error {
	if len(room) != 1 {
		return fmt.Errorf("invalid room: %#v", room)
	}
	c.Count = room[0]
	return nil
}
