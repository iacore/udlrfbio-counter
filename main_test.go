package main

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"os"
	"sync"
	"testing"
	"time"
)

type DummyDeadlineSetter struct{}

func (DummyDeadlineSetter) SetReadDeadline(t time.Time) error  { return nil }
func (DummyDeadlineSetter) SetWriteDeadline(t time.Time) error { return nil }

func testIOCustomLogger(t *testing.T, logger *log.Logger, input []byte) ([]byte, error) {
	r := bytes.NewReader(input)
	w := bytes.NewBuffer(nil)
	c := Counter{Count: 0, Logger: logger}
	err := HandleConnection(&c, logger, DummyDeadlineSetter{}, r, w, 0)

	return w.Bytes(), err
}

func testIO(t *testing.T, input []byte) ([]byte, error) {
	var (
		buf    bytes.Buffer
		logger = log.New(&buf, fmt.Sprintf("%v: ", t.Name()), log.Lshortfile)
	)

	bytes, err := testIOCustomLogger(t, logger, input)
	if err != nil {
		err = fmt.Errorf("%w\nlog: %v", err, buf.Bytes())
	}
	return bytes, err
}

func TestExampleInSpec(t *testing.T) {
	input := []byte("\x10\x18\x08\x08\x08\x18\x11\x01\x00")
	output := []byte("\x01\x00\x01\x01\x01\x05\x01\x00")
	actual_output, err := testIO(t, input)
	if err != nil {
		t.Fatalf("%v", err)
	}
	if !bytes.Equal(actual_output, output) {
		t.Fatalf("%#v != %#v", actual_output, output)
	}
}

func FuzzCounter(f *testing.F) {
	log_file, err := os.OpenFile("fuzz-counter.log", os.O_CREATE|os.O_APPEND, 0640)
	f.Cleanup(func() { _ = log_file.Close() })

	if err != nil {
		f.Fatal(err)
	}
	w := bufio.NewWriter(log_file)
	defer w.Flush()

	seen_errors := make(map[string]bool)
	var m sync.Mutex

	input := []byte("\x10\x18\x08\x08\x08\x18\x11\x01\x00")
	f.Add(input)
	f.Fuzz(func(t *testing.T, input []byte) {
		m.Lock()
		defer m.Unlock()
		logger := log.New(w, fmt.Sprintf("%v: ", t.Name()), log.Lshortfile)
		actual_output, err := testIOCustomLogger(t, logger, input)
		if err != nil && !seen_errors[err.Error()] {
			seen_errors[err.Error()] = true
			fmt.Fprintf(w, "in = %#v | out = %#v | err = %v\n", input, actual_output, err)
		}
	})
}
