package main

import (
	"errors"
	"fmt"
	"io"
	"log"
	"time"
)

type MoveDir uint8

const (
	U MoveDir = iota + 8
	D
	L
	R
	F
	B
	I
	O
	nop      = 0
	teleport = 1
)

func AllNormalMovements() [8]MoveDir {
	return [8]MoveDir{U, D, L, R, F, B, I, O}
}

func HandleConnection(place PlaceMut, logger *log.Logger, conn DeadlineSetter, r io.ByteReader, w io.ByteWriter, idleTimeout time.Duration) (ret_err error) {
	defer func() {
		if errors.Is(ret_err, io.EOF) {
			ret_err = nil
		}
	}()

	for {
		if err := conn.SetReadDeadline(time.Now().Add(idleTimeout)); err != nil {
			return err
		}
		command, err := r.ReadByte()
		if err != nil {
			return err
		}

		movement := MoveDir(command & 0b00001111)
		switch movement {
		case nop:
		case teleport:
			room, err := ReadSlice(r)
			if err != nil {
				return err
			}
			place.Teleport(room)
		case U, D, L, R, F, B, I, O:
			place.Move(movement)
		default:
			return fmt.Errorf("invalid move: %x from command %x", movement, command)
		}
	
		tellWhere := command&0b00010000 != 0

		if tellWhere {
			if err := conn.SetWriteDeadline(time.Now().Add(idleTimeout)); err != nil {
				return err
			}
			if err := WriteSlice(w, place.Current()); err != nil {
				return err
			}
		}
	}
}

type Room []byte

type PlaceMut interface {
	// the function should copy the slice if it intend to use it
	Teleport(Room) error
	// should make a copy of it
	Current() Room
	Move(MoveDir)
}
